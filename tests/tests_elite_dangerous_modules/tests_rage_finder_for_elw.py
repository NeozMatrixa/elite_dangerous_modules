#!usr/bin/env python3

__author__ = "Radosław Jachimowicz"
__copyright__ = "Copyright (c) 2020 Radosław Jachimowicz"
__email__ = "radjach@gmail.com"
__version__ = "1.5"

import unittest
from elite_dangerous_modules.rage_finder_for_elw import RageFinderForELW
from pathlib import Path


class TestXlsxParse(unittest.TestCase):
    def test_module(self):
        filename = Path("tests/tests_elite_dangerous_modules/tests_elw2.xlsx")
        RageFinderForELW(18457, 18464, filename)


if __name__ == '__main__':
    unittest.main()
