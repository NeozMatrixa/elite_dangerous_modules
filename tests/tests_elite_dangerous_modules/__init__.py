#!usr/bin/env python3

__author__ = "Radosław Jachimowicz"
__copyright__ = "Copyright (c) 2020 Radosław Jachimowicz"
__email__ = "radjach@gmail.com"
__version__ = "1.5"

import unittest

if __name__ == '__main__':
    unittest.main()
