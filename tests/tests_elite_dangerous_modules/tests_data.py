#!usr/bin/env python3

__author__ = "Radosław Jachimowicz"
__copyright__ = "Copyright (c) 2020 Radosław Jachimowicz"
__email__ = "radjach@gmail.com"
__version__ = "1.5"

import unittest
import xlrd
from pathlib import Path


class TestXlsxParse(unittest.TestCase):
    def test_for_data_in_module(self):
        filename = Path("tests/tests_elite_dangerous_modules/tests_elw2.xlsx")
        book = xlrd.open_workbook(filename)
        sheet = book.sheet_by_index(0)
        for num in range(1, 18457):
            print(f"ID: {str(num)} {sheet.cell_value(num, 0)}" + f": {sheet.cell_value(num, 2)}ly <-- TEST OK")


if __name__ == '__main__':
    unittest.main()
