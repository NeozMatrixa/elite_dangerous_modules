#!usr/bin/env python3

__author__ = "Radosław Jachimowicz"
__copyright__ = "Copyright (c) 2020 Radosław Jachimowicz"
__email__ = "radjach@gmail.com"
__version__ = "1.5"

import xlrd


class ELWdata:
    def __init__(self, min_r: int, max_r: int, path):
        """
        Open and read an Excel file
        """
        self._min_r = min_r
        self._max_r = max_r
        self._path = path

        book = xlrd.open_workbook(path)
        sheet = book.sheet_by_index(0)

        for num in range(min_r, max_r):
            print('\033[94m' + sheet.cell_value(num, 0) + '\033[92m' f": {sheet.cell_value(num, 2)}ly")

        print('\033[0;32m' + "\nDONE!!!")
