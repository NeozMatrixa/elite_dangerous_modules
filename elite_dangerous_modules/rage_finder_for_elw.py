#!usr/bin/env python3

__author__ = "Radosław Jachimowicz"
__copyright__ = "Copyright (c) 2020 Radosław Jachimowicz"
__email__ = "radjach@gmail.com"
__version__ = "1.5"

from elite_dangerous_modules.data import ELWdata


class RageFinderForELW:
    def __init__(self, min_r, max_r, path):
        self._min_r = min_r
        self._max_r = max_r
        self._path = path

        print("\n!!!This module prints list of Earth-like worlds sorted by index from closest one the one furthest!!!")
        print("Latest Update(22.05.2020): 18457 indexed Earth-like worlds in milky way\n")
        correct: bool = False
        while not correct:
            print('\033[95m')
            if max_r > 18464 or max_r <= min_r or min_r <= 0 or min_r > 18464:
                print("You typed number out of index or min value exceed max!!!\nTry again ;)\n")
            else:
                print("Please wait...")
                ELWdata(min_r, max_r, path)
                correct = True

    @property
    def min_r(self):
        return self._min_r

    @property
    def max_r(self):
        return self._max_r

    @property
    def path(self):
        return self._path
