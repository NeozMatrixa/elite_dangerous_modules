import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="elite_dangerous_modules-Radek",
    version="0.1.6",
    author="Radosław Jachimowicz",
    author_email="radjach@gmail.com",
    description="Module for Game Elite Dangerous.inc",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/NeozMatrixa/elite_dangerous_modules",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)