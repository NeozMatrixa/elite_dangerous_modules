# Elite Dangerous_modules [![pipeline status](https://gitlab.com/NeozMatrixa/elite_dangerous_modules/badges/master/pipeline.svg)](https://gitlab.com/NeozMatrixa/elite_dangerous_modules/-/commits/master) [![coverage report](https://gitlab.com/NeozMatrixa/elite_dangerous_modules/badges/master/coverage.svg)](https://gitlab.com/NeozMatrixa/elite_dangerous_modules/-/commits/master)

## List of modules

### **Earth-like worlds**
![alt text][sol]

## Authors

![alt text][logo]
**Radosław Jachimowicz**
========================

See also the list of [contributors](https://gitlab.com/NeozMatrixa/ap2020homework/-/graphs/master) who participated in this project.

## Acknowledgments

*  [marx](https://forums.frontier.co.uk/threads/list-of-earth-like-worlds-v2.168287/)
*  [Draqun](https://github.com/Draqun/python_good_practices)
*  [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
*  [adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


[logo]: https://avatars1.githubusercontent.com/u/48450331?s=100&amp;u=0f1303f8e3ddfbd6746c961de2f8a644ca50ada0&amp;v=4 "Siemanko :)"
[sol]: https://thecakeisaliegaming.files.wordpress.com/2014/12/galaxay-map-route-planner.jpg "Sol_system"